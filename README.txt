Human Resources Management System

Project page: https://www.drupal.org/sandbox/jianhe/2847047

Synopsis
--------
This project enables the efficient management of workforce data and supports
all standard HR activities, including workforce organization, development, and
measurement.

Requirements
------------
business_menu, business_toolbar and other modules from business_core:
https://www.drupal.org/sandbox/jianhe/2846751

Roadmap
-------
https://www.drupal.org/node/2847242

Dependencies
------------
Drupal 8.x

Modules that depends on human_resources
----------------------------------------
Projects management system(https://www.drupal.org/sandbox/jianhe/2847056)

Clone command
-------------
git clone --branch 8.x-2.x jianhe@git.drupal.org:sandbox/jianhe/2847047.git human_resources

pareview.sh
-----------
https://pareview.sh/node/925
